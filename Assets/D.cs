﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D : RotationPoll {

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!active) return;
        rotate(AXIS.Z);
        //perlinNoise(AXIS.Y);
        //perlinNoise(AXIS.Z);
    }

    override public string getName()
    {
        return "D";
    }
}
